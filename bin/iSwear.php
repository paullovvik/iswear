#!/usr/bin/env php
<?php
use iSwear\iSwear;

include dirname(__FILE__) . '/../vendor/autoload.php';

try {
  $i_swear = new iSwear();
  $i_swear->processLoop();
} catch (\Exception $e) {
  printf("Problem in iSwear script: %s\n", $e->getMessage());
  printf("%s\n", $e->getTraceAsString());
  exit(1);
}
