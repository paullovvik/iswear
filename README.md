iSwear
=====

This is an application designed to be executed on a Raspberry Pi.

Imagine being in flow when something horrible happens. Rather than pause to think of a witty outburst, simply hit a button and the outburst is calculated and let fly completely asynchronous to your work.

Setup
=====

*These setup instructions are untested. Please edit as necessary.*

- Install required packages:

`sudo apt-get install vorbis-tools festival mysql-server php5-common php5-cli php5-mysql`

NOTE: Do not set a root password for mysql.

- Create a MySQL database:

`mysqladmin -uroot create iswear`

- Install composer:

`wget https://getcomposer.org/composer.phar`
`chmod +x composer.phar`
sudo mv composer.phar /usr/bin/composer`

- Check out both repositories (note that they must live in /home/pi):

`git clone https://bitbucket.org/paullovvik/iswear.git ~/iSwear`
`git clone https://bitbucket.org/paullovvik/iswearmedia.git ~/iSwearMedia`

- Symlink the repositories to /root:

`ln -s /home/pi/iswear /root/iSwear`
`ln -s /home/pi/iswearmedia /root/iSwearMedia`

- Pull in dependencies via composer:

`cd ~/iSwear`
`composer install`

- Set up two cron jobs as follows, to pull in code updates from bitbucket every five minutes:

`crontab -e`

`*/5 * * * * cd /root/iSwear && /root/iSwear/bin/updateRepo.sh --overwrite >> /tmp/iSwearUpdate.log 2>&1`
`*/5 * * * * cd /root/iSwearMedia && /root/iSwear/bin/updateRepo.sh --overwrite >> /tmp/iSwearMediaUpdate.log 2>&1`

- Configure the iswear, weswear, and button monitors:
cp init.d/* /etc/init.d
pushd /etc/rc4.d
ln -s /etc/init.d/iSwear S04iSwear
ln -s /etc/init.d/weSwear S04weSwear
ln -s /etc/init.d/iSwearButtons S04iSwearButtons
pushd

- Copy the iSwear.desktop to the desktop for easy access.
cp bin/iSwear.desktop ~pi/Desktop
chown pi:pi ~pi/Desktop/iSwear.desktop


- Reboot
- (If you are using the UI) Start the UI:
    * Double-click the iSwear icon on the desktop
    * If preferred you can launch it on the command line:
        /root/iSwear/bin/run-ui.sh

- Push a button!
