package com.acquia.iswear.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicButtonListener;

import com.acquia.iswear.util.SwearAgent;

public class SwearBox {
	private static int MIN_MESSAGE_LEVEL = 0;
	private static int MAX_MESSAGE_LEVEL = 100;
	private static int MIN_DELAY = 0;
	private static int MAX_DELAY = 100;
	
	private static int MESSAGE_LEVEL = MAX_MESSAGE_LEVEL;
	private static int DELAY = MIN_DELAY;
	
	private static String MESSAGE_LEVEL_LABEL = "Profanity Tolerance: ";
	private static String DELAY_LABEl = "Render delay (seconds): ";
	private static String HOME_DIR = System.getProperty("user.home");
	
	private static boolean CONFIG_CHANGED = false;
	
	/**
	 * Creates the buttons panel.
	 * 
	 * @param panel
	 */
	public static void createButtonsPanel(JPanel panel) {
		SwearAgent agent = new SwearAgent();
		
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
	    c.fill = GridBagConstraints.BOTH;	    
	    c.weightx = 1;
	    c.weighty = 1;
	    c.gridwidth = 2;
	    c.gridheight = 2;
		
		// Add the button components. All button icons were created by Angela Byron.
	    ImageIcon happyIcon = new ImageIcon(SwearBox.class.getResource("/img/happy.png"));
		JButton happyButton = new JButton(happyIcon);

		happyButton.setOpaque(true);
		MouseListener happyButtonListener = new BasicButtonListener(happyButton) {
			private double duration = 0; 
			private long startTime = 0;
			
			public void mousePressed(MouseEvent e) {
				startTime = System.currentTimeMillis();
			}
			
			public void mouseReleased(MouseEvent e) {
				duration = (double) (System.currentTimeMillis() - startTime);
				agent.say(SwearAgent.Emotion.HAPPY, duration, MESSAGE_LEVEL);
			}
		};
		happyButton.addMouseListener(happyButtonListener);
		happyButton.setPreferredSize(new Dimension(80, 60));
	    c.gridx = 0;
	    c.gridy = 0;
	    panel.add(happyButton, c);
		
		JButton angryButton = new JButton(new ImageIcon(SwearBox.class.getResource("/img/angry.png")));
		angryButton.setPreferredSize(new Dimension(80, 60));
		MouseListener angryButtonListener = new BasicButtonListener(angryButton) {
			private double duration = 0; 
			private long startTime = 0;
			
			public void mousePressed(MouseEvent e) {
				startTime = System.currentTimeMillis();
			}
			
			public void mouseReleased(MouseEvent e) {
				duration = (double) (System.currentTimeMillis() - startTime);
				agent.say(SwearAgent.Emotion.ANGRY, duration, MESSAGE_LEVEL);
			}
		};
		angryButton.addMouseListener(angryButtonListener);	    
	    c.gridx = 3;
	    c.gridy = 0;
	    panel.add(angryButton, c);
		
		JButton panicButton = new JButton(new ImageIcon(SwearBox.class.getResource("/img/panic.png")));
		panicButton.setPreferredSize(new Dimension(80, 60));
		MouseListener panicButtonListener = new BasicButtonListener(panicButton) {
			private double duration = 0; 
			private long startTime = 0;
			
			public void mousePressed(MouseEvent e) {
				startTime = System.currentTimeMillis();
			}
			
			public void mouseReleased(MouseEvent e) {
				duration = (double) (System.currentTimeMillis() - startTime);
				agent.say(SwearAgent.Emotion.PANIC, duration, MESSAGE_LEVEL);
			}
		};
		panicButton.addMouseListener(panicButtonListener);
	    c.gridx = 0;
	    c.gridy = 3;
	    panel.add(panicButton, c);
		
 		JButton randomButton = new JButton(new ImageIcon(SwearBox.class.getResource("/img/random.png")));
 		randomButton.setPreferredSize(new Dimension(80, 60));
		MouseListener randomButtonListener = new BasicButtonListener(randomButton) {
			private double duration = 0; 
			private long startTime = 0;
			
			public void mousePressed(MouseEvent e) {
				startTime = System.currentTimeMillis();
			}
			
			public void mouseReleased(MouseEvent e) {
				duration = (double) (System.currentTimeMillis() - startTime);
				agent.say(SwearAgent.Emotion.RANDOM, duration, MESSAGE_LEVEL);
			}
		};
		randomButton.addMouseListener(randomButtonListener);
	    c.gridx = 3;
	    c.gridy = 3;
	    panel.add(randomButton, c);
	}
	/**
	 * Creates the settings panel.
	 * 
	 * @param panel
	 */
	public static void createSettingsPanel(JPanel panel) {
		panel.setBorder(new EmptyBorder(0, 0, 0, 0));
        GridLayout layout = new GridLayout(2, 1);
        layout.setVgap(0);
        panel.setLayout(layout);
        
        // Message level
        JPanel messageLevelPanel = new JPanel();
        GridLayout messageLevelLayout = new GridLayout(2, 1);
        messageLevelLayout.setVgap(0);
        messageLevelPanel.setLayout(messageLevelLayout);
        messageLevelPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        
        JLabel messageLevelTitle = new JLabel(MESSAGE_LEVEL_LABEL + MESSAGE_LEVEL);
        messageLevelTitle.setBorder(new EmptyBorder(0, 0, 0, 0));
        messageLevelTitle.setHorizontalAlignment(JLabel.LEFT);
   
        JSlider messageLevelSlider = new JSlider(JSlider.HORIZONTAL, MIN_MESSAGE_LEVEL, MAX_MESSAGE_LEVEL, MESSAGE_LEVEL);
        Hashtable<Integer, JLabel> messageLevelLabelTable = new Hashtable<Integer, JLabel>();
        for (int i = 0; i <= 100; i+=20) {
        	messageLevelLabelTable.put(new Integer(i), new JLabel(i  + ""));
        }
        messageLevelSlider.setLabelTable(messageLevelLabelTable);
        messageLevelSlider.setPaintLabels(true);
        messageLevelSlider.addChangeListener(new ChangeListener() {
        	public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider)e.getSource();
                if (!source.getValueIsAdjusting()) {
                    int messageLevel = (int)source.getValue();
                    if (messageLevel == 0) {
                    	messageLevel = 1;
                    } 
                	messageLevelTitle.setText(MESSAGE_LEVEL_LABEL + messageLevel);
                	MESSAGE_LEVEL = messageLevel;
                	CONFIG_CHANGED = true;
                }
            }
        });

        messageLevelPanel.add(messageLevelTitle);
        messageLevelPanel.add(messageLevelSlider);
        
        // Delay
        JPanel delayPanel = new JPanel();
        GridLayout delayLayout = new GridLayout(2, 1);
        delayLayout.setVgap(0);
        delayPanel.setLayout(delayLayout);
        delayPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        
        JLabel delayTitle = new JLabel(DELAY_LABEl + getDelayValueFromStorageValue(DELAY));
        delayTitle.setBorder(new EmptyBorder(0, 0, 0, 0));
        delayTitle.setHorizontalAlignment(JLabel.LEFT);
   
        JSlider delaySlider = new JSlider(JSlider.HORIZONTAL, MIN_DELAY, MAX_DELAY, DELAY);
        Hashtable<Integer, JLabel> delayLabelTable = new Hashtable<Integer, JLabel>();
        for (int i = 0; i <= 100; i+=20) {
        	delayLabelTable.put(new Integer(i), new JLabel(((double) i)/10 + ""));
        }
        delaySlider.setLabelTable(delayLabelTable);
        delaySlider.setPaintLabels(true);
        delaySlider.addChangeListener(new ChangeListener() {
        	public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider)e.getSource();
                if (!source.getValueIsAdjusting()) {
                    int delayAmount = (int) source.getValue();
                	delayTitle.setText(DELAY_LABEl + getDelayValueFromStorageValue(delayAmount));
                	DELAY = delayAmount;
                	CONFIG_CHANGED = true;
                }
            }
        });
        
        delayPanel.add(delayTitle);
        delayPanel.add(delaySlider);
        
        panel.add(messageLevelPanel);
        panel.add(delayPanel);
	}
	
	/**
     * Creates the GUI and shows it.
     */
    private static void createAndShowGUI() {
    	UIManager.getDefaults().put("TabbedPane.contentBorderInsets", new Insets(0,0,0,0));
        // Create and set up the window.
        JFrame frame = new JFrame("iSwear");
        frame.setSize(320, 240);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setIconImage(new ImageIcon(SwearBox.class.getResource("/img/angry.png")).getImage());
 
        // Create the tabbed pane.
        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.LEFT, JTabbedPane.SCROLL_TAB_LAYOUT);
        tabbedPane.addChangeListener(new ChangeListener() {
        	public void stateChanged(ChangeEvent e) {
        		if (tabbedPane.getSelectedIndex() == 0 && CONFIG_CHANGED) {
        			CONFIG_CHANGED = false;
        			// Save values from settings.
        			saveConfig();
        		}
	          }
        });
        
        // Set up the content panel.
        JPanel buttonPanel = new JPanel(false);
        buttonPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        createButtonsPanel(buttonPanel);
        
        // Set up the settings panel.
        JPanel settingsPanel = new JPanel(false);
        createSettingsPanel(settingsPanel);
        
        // Speaker by Mete Eraydın from thenounproject.com
        ImageIcon speakerIcon = new ImageIcon(SwearBox.class.getResource("/img/speaker.png"));
        // Gear by Icomatic from thenounproject.com
        ImageIcon settingsIcon = new ImageIcon(SwearBox.class.getResource("/img/settings.png"));
        
        tabbedPane.addTab(null, speakerIcon, buttonPanel);
        tabbedPane.addTab(null, settingsIcon, settingsPanel);
        
        frame.add(tabbedPane);
        frame.setVisible(true);
    }
    
    public static void setup() {
    	String configFileName = HOME_DIR + "/config.ini";
		List<String> list = new ArrayList<>();

		//read file into stream, try-with-resources
		try (Stream<String> stream = Files.lines(Paths.get(configFileName))) {
			list = stream.collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for (String config : list) {
			if (config.startsWith("maximumDegree")) {
				String[] parts = config.split(" = ");
				MESSAGE_LEVEL = Integer.parseInt(parts[1]);
			} else if (config.startsWith("renderDelay")) {
				String[] parts = config.split(" = ");
				DELAY = getStorageValueFromDelayValue(Double.parseDouble(parts[1]));
			}
		}
    }
    
    public static void saveConfig() {
    	String configFileName = HOME_DIR + "/config.ini";
    	String configChangedFileName = HOME_DIR + "/.restart_requested";
		List<String> input = new ArrayList<>();
		List<String> output = new ArrayList<>();
		
		String maximumDegree = "maximumDegree = " + MESSAGE_LEVEL;
		String renderDelay = "renderDelay = " + getDelayValueFromStorageValue(DELAY);

		//read file into stream, try-with-resources
		try (Stream<String> stream = Files.lines(Paths.get(configFileName))) {
			input = stream.collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		for (String config : input) {
			if (config.startsWith("maximumDegree")) {
				output.add(maximumDegree);
			} else if (config.startsWith("renderDelay")) {
				output.add(renderDelay);
			} else {
				output.add(config.trim());
			}
		}

		try {
			Files.write(Paths.get(configFileName), output);
			// Flag to notify the back-end of config changes.
		    File file = new File(configChangedFileName);
		    file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    private static double getDelayValueFromStorageValue(int val) {
    	return (double) val / 10;
    }
    
    private static int getStorageValueFromDelayValue(double val) {
    	return (int) (val * 10.0);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	setup();
                createAndShowGUI();
            }
        });
    }
}
