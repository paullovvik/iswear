package com.acquia.iswear.util;

import java.io.IOException;

public class SwearAgent {
	public static final int ANGRY = 0;
	public static final int HAPPY = 1;
	public static final int PANIC = 2;
	public static final int RANDOM = 3;	
	
	public enum Emotion {
	    ANGRY("angry"), 
	    HAPPY("happy"), 
	    PANIC("panic"), 
	    RANDOM("random");
		
		private String name;
		Emotion(String emotion) {
	        this.name = emotion;
	    }
		
		static boolean isValid(String value) {
			for (Emotion e : Emotion.values()) {
				if (e.name.equalsIgnoreCase(value)) {
					return true;
				}
			}
			return false;
		}
	}
	
	// Panic, Non Sequitur, Angry, Happy
	public SwearAgent() {}
	
	public void say(Emotion emotion, double buttonDuration, int maxMessageLevel) {
		if (!Emotion.isValid(emotion.name)) {
			throw new IllegalArgumentException("Invalid emotion type.");
		}
		
		int level = this.convertToMessageLevel(buttonDuration);
		level = level > maxMessageLevel? maxMessageLevel : level;
		this.callEnqueue(emotion.name, level);
	}
	
	
	private int convertToMessageLevel(double buttonDuration) {
		double degree = 100.0 * Math.log10(buttonDuration / 300.0);
		
		if (degree > 100.0) {
			return 100;
		}
		if (degree < 1.0) {
			return 1;
		}
		
		return (int) degree;
	}
	
	private void callEnqueue(String emotion, int level) {
		// TODO: implement call here.
		String command = String.format("%s/iSwear/bin/enqueueSwear --degree=%d --emotion=%s", System.getProperty("user.home"), level, emotion);
		try {
			Runtime.getRuntime().exec(command);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
