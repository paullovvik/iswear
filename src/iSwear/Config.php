<?php

namespace iSwear;

class Config {

  /**
   * The configuration data, as an associative array.
   *
   * @var array
   */
  protected $data;

  public function __construct(array $data) {
    $this->data = $data;
  }

  /**
   * Magic getter.
   *
   * @param string $name
   *   The config item name.
   * @return string|null
   */
  public function __get($name) {
    $result = NULL;
    if (!empty($this->data[$name])) {
      $result = $this->data[$name];
    }
    return $result;
  }

  /**
   * Gets the user's maximum outburst tolerance.
   *
   * @return int
   *   The maximum tolerance.
   */
  public function getUserMaximumDegree() {
    $result = 100;
    if (!empty($this->data['maximumDegree'])) {
      $max = $this->data['maximumDegree'];
      if (is_numeric($max) && $max > 0 && $max <= 100) {
        $result = intval($max);
      } else {
        throw new \DomainException('The config file contains an illegal value for the maximumDegree property.');
      }
    }
    return intval($result);
  }

  /**
   * Gets the render delay.
   *
   * This is the delay between receiving an outburst from the group and the
   * time that the outburst is actually rendered. Normally this should be zero
   * but for demonstrations it can be useful to delay one of the devices.
   */
  public function getRenderDelay() {
    $result = 0.0;
    if (!empty($this->data['renderDelay'])) {
      $delay = $this->data['renderDelay'];
      if (is_numeric($delay) && $delay > 0) {
        $result = floatval($delay);
      }
    }
    return $result;
  }

  /**
   * Returns the home directory for this iSwear installation.
   *
   * @return string
   */
  public static function getHomeDirectory() {
    return getenv('HOME');
  }

  /**
   * Returns the installation directory for the iSwear application.
   *
   * @return string
   */
  public static function getISwearDirectory() {
    return static::getHomeDirectory() . '/iswear';
  }

  /**
   * Returns the directory in which the iSwear media is located.
   *
   * @return string
   */
  public static function getMediaDirectory() {
    return static::getHomeDirectory() . '/iSwearMedia';
  }

  /**
   * Returns a Config object for this iSwear installation.
   *
   * @return Config
   * @throws \Exception
   */
  public static function create() {
    $path = static::getHomeDirectory();
    return static::createFromFile($path . '/config.ini');
  }

  /**
   * Returns a Config object for a given file.
   *
   * @param string $path
   *   The path to the configuration file.
   * @return Config
   * @throws \Exception
   */
  protected static function createFromFile($path) {
    if (!file_exists($path)) {
      throw new \Exception('Configuration file not found at ' . $path);
    }
    $data = parse_ini_file($path, TRUE);
    if ($data) {
      return new static($data);
    }
    else {
      throw new \Exception("Configuration file at $path is in an unknown format.");
    }
  }

  /*
   * Retrieves the current user's name from the config.
   *
   * @returns string|NULL
   *   The user's name, if it exists, else NULL.
   */
  public function getName() {
    $name = NULL;
    if (array_key_exists('name', $this->data)) {
      $name = $this->data['name'];
    }
    return $name;
  }

  /*
   * Retrieves the current user's nameTts from the config.
   *
   * @returns string|NULL
   *   The string value of nameTts, if it exists, else NULL.
   */
  public function getNameTts() {
    $name_tts = NULL;
    if (array_key_exists('nameTts', $this->data)) {
      $name_tts = $this->data['nameTts'];
    }
    return $name_tts;
  }

  /**
   * Gets the IP address to which outbursts will be broadcast.
   *
   * @return string
   *   The IP address.
   */
  public function getBroadcastIp() {
    $result = '52.91.0.71';
    if (!empty($this->data['broadcastIp'])) {
      $result = $this->data['broadcastIp'];
    }
    return $result;
  }

  /**
   * Gets the port number to which outbursts will be broadcast.
   *
   * @return int
   *   The port number.
   */
  public function getBroadcastPort() {
    $result = 5555;
    if (!empty($this->data['broadcastPort'])) {
      $result = intval($this->data['broadcastPort']);
    }
    return $result;
  }

  /**
   * Gets the IP address from which outbursts will be received.
   *
   * @return string
   *   The IP address.
   */
  public function getReceiveIp() {
    $result = '52.91.0.71';
    if (!empty($this->data['receiveIp'])) {
      $result = $this->data['receiveIp'];
    }
    return $result;
  }

  /**
   * Gets the port number from which outbursts will be received.
   *
   * @return int
   *   The port number.
   */
  public function getReceivePort() {
    $result = 4444;
    if (!empty($this->data['receivePort'])) {
      $result = intval($this->data['receivePort']);
    }
    return $result;
  }

  /**
   * Gets the name of the iSwear database.
   *
   * @return string
   *   The database name.
   */
  public function getDatabaseName() {
    $result = 'iswear';
    if (!empty($this->data['databaseName'])) {
      $result = $this->data['databaseName'];
    }
    return $result;
  }

  /**
   * Gets the name of the iSwear server.
   *
   * @return string
   *   The server name.
   */
  public function getDatabaseServer() {
    $result = 'localhost';
    if (!empty($this->data['databaseServer'])) {
      $result = $this->data['databaseServer'];
    }
    return $result;
  }

  /**
   * Gets the database user name.
   *
   * @return string
   *   The user name.
   */
  public function getDatabaseUser() {
    $result = 'root';
    if (!empty($this->data['databaseUser'])) {
      $result = $this->data['databaseUser'];
    }
    return $result;
  }

  /**
   * Gets the database password.
   *
   * @return string
   *   The password.
   */
  public function getDatabasePassword() {
    $result = '';
    if (!empty($this->data['databasePassword'])) {
      $result = $this->data['databasePassword'];
    }
    return $result;
  }

}
