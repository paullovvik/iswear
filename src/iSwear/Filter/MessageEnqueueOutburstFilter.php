<?php

namespace iSwear\Filter;

use iSwear\Config;
use iSwear\OutburstFilterInterface;
use iSwear\OutburstInterface;
use weSwear\Broadcast;

/**
 * Responsible for queuing the populated outburst to the group.
 *
 * Note that this must occur after the outburst selection has been made.
 * Also this filter should only be added for outbursts that originated
 * locally.
 */
class MessageEnqueueOutburstFilter implements OutburstFilterInterface {

  /**
   * The application configuration.
   *
   * @var Config
   */
  private $config;

  public function __construct(Config $config) {
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public function filter(OutburstInterface $outburst) {
    $this->enqueue($outburst);
    return $outburst;
  }

  private function enqueue(OutburstInterface $outburst) {
    $broadcast_ip = $this->config->getBroadcastIp();
    $broadcast_port = $this->config->getBroadcastPort();
    $client = new Broadcast($broadcast_ip, $broadcast_port);
    $client->connect();
    $json = $outburst->toJson();
    $client->write($json);
    $client->close();
  }
}
