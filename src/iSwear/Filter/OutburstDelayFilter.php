<?php

namespace iSwear\Filter;

use iSwear\OutburstFilterInterface;
use iSwear\OutburstInterface;

/**
 * Inserts a delay between outburst and render according to the user's config.
 */
class OutburstDelayFilter implements OutburstFilterInterface {

  /**
   * The requested delay between receiving an outburst and the render step.
   *
   * @var float
   */
  private $delay = 0.0;

  /**
   * Initializes with the specified delay, measured in seconds.
   * @param float $delay
   */
  public function __construct($delay = 0.0) {
    if (!is_numeric($delay) || $delay < 0) {
      throw new \InvalidArgumentException('The delay must be a positive float value.');
    }
    $this->delay = floatval($delay);
  }

  /**
   * {@inheritdoc}
   */
  public function filter(OutburstInterface $outburst) {
    usleep($this->secondsToMicroseconds($this->delay));
    return $outburst;
  }

  /**
   * Converts the specified number of seconds into microseconds.
   *
   * @param float $seconds
   *   The number of seconds.
   *
   * @return int
   *   The number of microseconds.
   */
  public function secondsToMicroseconds($seconds) {
    return intval($seconds * 1000000);
  }
}
