<?php

namespace iSwear;

/**
 * This interface describes all outbursts.
 */
interface OutburstInterface {

  const MEDIA_TYPE_AUDIO = 1;
  const MEDIA_TYPE_TTS = 2;
  const MEDIA_TYPE_TTS_TEMPLATE = 3;

  /**
   * Indicates the outburst type is panic.
   *
   * This is used for outbursts that are negative to such a degree that they
   * cannot be expressed in words.
   */
  const PANIC = 1;

  /**
   * Indicates the outburst type is angry.
   *
   * This is used for outbursts that are negative in nature.
   */
  const ANGRY = 2;

  /**
   * Indicates the outburst type is happy.
   *
   * This is used for outbursts that are positive in nature.
   */
  const HAPPY = 3;

  /**
   * Sets the outburst identifier.
   *
   * @param int $id
   */
  public function setId($id);

  /**
   * Gets the outburst identifier.
   *
   * @return int
   *   The outburst identifier.
   */
  public function getId();

  /**
   * Sets the timestamp that indicates the approximate time of the outburst.
   *
   * @param int $timestamp
   *   The Unix timestamp.
   */
  public function setTimestamp($timestamp);

  /**
   * Gets the timestamp that indicates the approximate time of the outburst.
   *
   * @return int
   *   The Unix timestamp.
   */
  public function getTimestamp();

  /**
   * Sets the information pertaining to the owner of this outburst.
   *
   * @param string $owner
   *   The owner's name. This will be used for attribution.
   * @param string $tts_pronunciation
   *   Optional. If provided, this pronunciation will be used when performing
   *   text to speech.
   */
  public function setOwner($owner, $tts_pronunciation = NULL);

  /**
   * Gets the owner's name.
   *
   * @return string
   *   The owner.
   */
  public function getOwner();

  /**
   * Sets the owner's name using a pronunciation that sounds right using TTS.
   *
   * @param string $tts
   *   The TTS pronunciation.
   */
  public function setOwnerTTS($tts);

  /**
   * Gets the owner's name spelled such that TTS pronunciation is accurate.
   *
   * @return string
   *   The owner's name used for TTS. Note that if the owner's name has been
   *   set and the owner's TTS pronunciation has not, this method will return
   *   the owner's name.
   */
  public function getOwnerTTS();

  /**
   * Clears the owner info from this outburst instance.
   */
  public function clearOwner();

  /**
   * Sets the degree of severity of the outburst.
   *
   * This value is in the range of 1 - 100. The lower the value the less severe
   * the utterance.
   *
   * @param int $degree
   *   The degree of severity.
   */
  public function setDegree($degree);

  /**
   * Gets the degree of severity of the outburst.
   *
   * @return int
   *   An integer in the range of 1 - 100 indicating the severity.
   */
  public function getDegree();

  /**
   * Sets the type of this outburst.
   *
   * @param int $type
   *   The outburst type. This must be one of PANIC, FRUSTRATION, or POSITIVE.
   * @return mixed
   */
  public function setOutburstType($type);

  /**
   * Gets the type of this outburst.
   *
   * @return int
   *   The outburst type.
   */
  public function getOutburstType();

  /**
   * Sets the media type of this outburst.
   *
   * This indicates what type of media will be played to render the outburst
   * and must be one of MEDIA_TYPE_AUDIO, MEDIA_TYPE_TTS, or
   * MEDIA_TYPE_TTS_TEMPLATE.
   *
   * @param int $type
   *
   * @throws /InvalidArgumentException
   *   If the media type is invalid.
   */
  public function setMediaType($type);

  /**
   * Gets the media type.
   *
   * @return int
   *   The media type.
   */
  public function getMediaType();

  /**
   * The human-readable form of the outburst.
   *
   * @param string $text
   *   The human-readable outburst.
   */
  public function setText($text);

  /**
   * Gets the human-readable form of the outburst.
   *
   * @return string
   *   The outburst text.
   */
  public function getText();

  /**
   * Sets the media data associated with this outburst.
   *
   * Depending on the media type this could be the path to an audio file or a
   * TTS pronunciation.
   *
   * @param string $data
   *   The media data.
   */
  public function setMediaData($data);

  /**
   * Gets the media data associated with this outburst.
   *
   * @return string
   *   The media data.
   */
  public function getMediaData();

  /**
   * Sets the bit determining whether this is a locally-created outburst or not.
   *
   * @param bool $local
   */
  public function setLocal($local);

  /**
   * Determines whether or not this outburst is local.
   */
  public function getLocal();

  /**
   * Indicates whether the specified outburst is ready to be rendered.
   *
   * Often the outburst will not have sufficient information to be rendered.
   * This filter needs to know if there is work to be done.
   *
   * @param OutburstInterface $outburst
   *   The outburst.
   *
   * @return bool
   *   TRUE if the outburst is ready to be rendered; FALSE otherwise.
   */
  public function readyToRender(OutburstInterface $outburst);

  /**
   * Copies elements from the specified outburst to complete this instance.
   *
   * @param OutburstInterface $outburst
   *   The outburst to copy from.
   */
  public function copyFromOutburst(OutburstInterface $outburst);

  /**
   * Converts a string of the outburst emotion to a constant integer value.
   *
   * @param $type_string
   *   The human-readable version of the outburst emotion.
   * @return int
   *   The associated integer, if a valid emotion.
   *
   * @throws \InvalidArgumentException
   *   If the given type string is invalid.
   */
  public static function typeStringToConst($type_string);

  /**
   * Converts this Outburst instance into a JSON representation.
   *
   * @return string
   *   The JSON representation.
   */
  public function toJson();

  /**
   * Creates a new Outburst instance from the specified JSON data.
   *
   * @param string $json
   *   The JSON data.
   *
   * @return OutburstInterface
   *   The new Outburst instance.
   */
  public static function fromJson($json);
}
