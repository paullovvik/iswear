<?php

namespace iSwear;

class Outburst implements OutburstInterface {

  private $id;
  private $timestamp;
  private $owner;
  private $ownerTTS;
  private $degree;
  private $outburstType;
  private $mediaType;
  private $text;
  private $mediaData;
  private $local;

  /**
   * {@inheritdoc}
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function setTimestamp($timestamp) {
    $this->timestamp = $timestamp;
  }

  /**
   * {@inheritdoc}
   */
  public function getTimestamp() {
    return $this->timestamp;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner($owner, $tts_pronunciation = NULL) {
    $this->owner = $owner;
    if ($tts_pronunciation !== NULL) {
      $this->setOwnerTTS($tts_pronunciation);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->owner;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerTTS($tts) {
    $this->ownerTTS = $tts;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerTTS() {
    return $this->ownerTTS;
  }

  /**
   * {@inheritdoc}
   */
  public function clearOwner() {
    $this->owner = NULL;
    $this->ownerTTS = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setDegree($degree) {
    $this->degree = $degree;
  }

  /**
   * {@inheritdoc}
   */
  public function getDegree() {
    return $this->degree;
  }

  /**
   * {@inheritdoc}
   */
  public function setOutburstType($type) {
    $this->outburstType = $type;
  }

  /**
   * {@inheritdoc}
   */
  public function getOutburstType() {
    return $this->outburstType;
  }

  /**
   * {@inheritdoc}
   */
  public function setMediaType($type) {
    $this->mediaType = $type;
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaType() {
    return $this->mediaType;
  }

  /**
   * {@inheritdoc}
   */
  public function setText($text) {
    $this->text = $text;
  }

  /**
   * {@inheritdoc}
   */
  public function getText() {
    return $this->text;
  }

  /**
   * {@inheritdoc}
   */
  public function setMediaData($data) {
    $this->mediaData = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaData() {
    return $this->mediaData;
  }

  public static function getAllOutbursts() {
    $result = array();
    $file_path = Config::getMediaDirectory() . '/dictionary.ini';
    $outbursts = parse_ini_file($file_path, TRUE);
    foreach ($outbursts as $outburst_desc) {
      $outburst = self::createFromDesc($outburst_desc);
      self::addOutburst($outburst, $result);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function setLocal($local) {
    $this->local = (bool) $local;
  }

  /**
   * {@inheritdoc}
   */
  public function getLocal() {
    return (bool) $this->local;
  }

  /**
   * {@inheritdoc}
   */
  public function readyToRender(OutburstInterface $outburst) {
    $result = TRUE;
    if (
      empty($this->getMediaType())
      || empty($this->getMediaData())
    ) {
      $result = FALSE;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function copyFromOutburst(OutburstInterface $outburst) {
    $my_id = $this->getId();
    $outburst_id = $outburst->getId();
    if (empty($my_id) && !empty($outburst_id)) {
      $this->setId($outburst->getId());
    }
    $my_timestamp = $this->getTimestamp();
    $outburst_timestamp = $outburst->getTimestamp();
    if (empty($my_timestamp) && !empty($outburst_timestamp)) {
      $this->setTimestamp($outburst_timestamp);
    }
    $my_owner = $this->getOwner();
    $outburst_owner = $outburst->getOwner();
    if (empty($my_owner) && !empty($outburst->getOwner())) {
      $this->setOwner($outburst_owner);
    }
    $my_owner_tts = $this->getOwnerTTS();
    $outburst_owner_tts = $outburst->getOwnerTTS();
    if (empty($my_owner_tts) && !empty($outburst_owner_tts)) {
      $this->setOwnerTTS($outburst_owner_tts);
    }
    $my_degree = $this->getDegree();
    $outburst_degree = $outburst->getDegree();
    if (empty($my_degree) && !empty($outburst_degree)) {
      $this->setDegree($outburst_degree);
    }
    $my_outburst_type = $this->getOutburstType();
    $outburst_outburst_type = $outburst->getOutburstType();
    if (empty($my_outburst_type) && !empty($outburst_outburst_type)) {
      $this->setOutburstType($outburst_outburst_type);
    }
    $my_media_type = $this->getMediaType();
    $outburst_media_type = $outburst->getMediaType();
    if (empty($my_media_type) && !empty($outburst_media_type)) {
      $this->setMediaType($outburst_media_type);
    }
    $my_text = $this->getText();
    $outburst_text = $outburst->getText();
    if (empty($my_text) && !empty($outburst_text)) {
      $this->setText($outburst_text);
    }
    $my_media_data = $this->getMediaData();
    $outburst_media_data = $outburst->getMediaData();
    if (empty($my_media_data) && !empty($outburst_media_data)) {
      $this->setMediaData($outburst_media_data);
    }
  }

  private static function addOutburst(OutburstInterface $outburst, &$result) {
    $outburst_type = $outburst->getOutburstType();
    $degree = $outburst->getDegree();
    if (!isset($result[$outburst_type])) {
      $result[$outburst_type] = array();
    }
    if (!isset($result[$outburst_type][$degree])) {
      $result[$outburst_type][$degree] = array();
    }
    $result[$outburst_type][$degree][] = $outburst;
  }

  private static function createFromDesc($outburst_desc) {
    $result = new Outburst();
    $result->setOutburstType(self::typeStringToConst($outburst_desc['type']));
    $result->setDegree(intval($outburst_desc['degree']));
    $result->setMediaType(self::mediaTypeStringToConst($outburst_desc['mediatype']));
    $result->setText($outburst_desc['text']);
    $result->setMediaData($outburst_desc['resource']); // @todo - this needs to be changed in the other repo.
    return $result;
  }

  public static function typeStringToConst($type_string) {
    $type_string = strtolower($type_string);
    switch ($type_string) {
      case 'panic':
        $result = self::PANIC;
        break;
      case 'angry':
        $result = self::ANGRY;
        break;
      case 'happy':
        $result = self::HAPPY;
        break;
      default:
        throw new \InvalidArgumentException(sprintf('Unknown type %s.', $type_string));
    }
    return $result;
  }

  public static function mediaTypeStringToConst($media_type_string) {
    $media_type_string = strtolower($media_type_string);
    switch ($media_type_string) {
      case 'tts':
        $result = self::MEDIA_TYPE_TTS;
        break;
      case 'tts_template':
        $result = self::MEDIA_TYPE_TTS_TEMPLATE;
        break;
      case 'audio':
        $result = self::MEDIA_TYPE_AUDIO;
        break;

      default:
        throw new \InvalidArgumentException(sprintf('Unknown type %s.', $media_type_string));
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    $obj = new \stdClass();
    $timestamp = $this->getTimestamp();
    if (!empty($timestamp)) {
      $obj->timestamp = $timestamp;
    }
    $owner = $this->getOwner();
    if (!empty($owner)) {
      $obj->owner = $owner;
    }
    $owner_tts = $this->getOwnerTTS();
    if (!empty($owner_tts)) {
      $obj->ownerTTS = $owner_tts;
    }
    $degree = $this->getDegree();
    if (!empty($degree)) {
      $obj->degree = $degree;
    }
    $outburst_type = $this->getOutburstType();
    if (!empty($outburst_type)) {
      $obj->outburstType = $outburst_type;
    }
    $media_type = $this->getMediaType();
    if (!empty($media_type)) {
      $obj->mediaType = $media_type;
    }
    $text = $this->getText();
    if (!empty($text)) {
      $obj->text = $text;
    }
    $media_data = $this->getMediaData();
    if (!empty($media_type)) {
      $obj->mediaData = $media_data;
    }
    return json_encode($obj);
  }

  /**
   * {@inheritdoc}
   */
  public static function fromJson($json) {
    $obj = json_decode($json);
    if (NULL === $obj) {
      throw new \InvalidArgumentException('The specified JSON document is not valid.');
    }

    // Create a new outburst and populate it based on the json data.
    $result = new Outburst();

    if (!empty($obj->timestamp)) {
      $result->setTimestamp(intval($obj->timestamp));
    }
    if (!empty($obj->owner)) {
      $result->setOwner($obj->owner);
    }
    if (!empty($obj->ownerTTS)) {
      $result->setOwnerTTS($obj->ownerTTS);
    }
    if (!empty($obj->degree)) {
      $result->setDegree(intval($obj->degree));
    }
    if (!empty($obj->outburstType)){
      $result->setOutburstType(intval($obj->outburstType));
    }
    if (!empty($obj->mediaType)) {
      $result->setMediaType($obj->mediaType);
    }
    if (!empty($obj->text)) {
      $result->setText($obj->text);
    }
    if (!empty($obj->mediaData)) {
      $result->setMediaData($obj->mediaData);
    }
    return $result;
  }
}
