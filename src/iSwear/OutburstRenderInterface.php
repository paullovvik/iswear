<?php

namespace iSwear;

/**
 * This interface describes all classes that render outbursts.
 */
interface OutburstRenderInterface {

  /**
   * Renders the specified outburst.
   *
   * @param OutburstInterface $outburst
   *   The outburst to be rendered.
   */
  public function render(OutburstInterface $outburst);
}
