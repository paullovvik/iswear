<?php

namespace iSwear\Render;

use iSwear\OutburstInterface;
use iSwear\OutburstRenderInterface;

/**
 * Responsible for rendering an outburst as text-to-speech.
 */
class TtsOutburstRenderer implements OutburstRenderInterface {

  /**
   * {@inheritdoc}
   */
  public function render(OutburstInterface $outburst) {
    $tts_text = $this->getTtsText($outburst);
    if (!empty($tts_text)) {
      $command = sprintf('echo "%s" | festival --tts', $tts_text);
      exec($command);
    }
  }

  /**
   * Gets the text-to-speech string that will be rendered.
   *
   * @param OutburstInterface $outburst
   *   The outburst.
   *
   * @return string
   *   The outburst in a form suitable for text to speech software.
   */
  protected function getTtsText(OutburstInterface $outburst) {
    $media_type = $outburst->getMediaType();
    if (
      $media_type != OutburstInterface::MEDIA_TYPE_TTS
      && $media_type != OutburstInterface::MEDIA_TYPE_TTS_TEMPLATE
    ) {
      throw new \RuntimeException(sprintf('The media type is expected to be TTS or TTS_TEMPLATE. Got %s instead.', $media_type));
    }
    $result = $outburst->getMediaData();
    if (empty($result)) {
      // Fall back to using the text associated with the outburst.
      $result = $outburst->getText();
    }
    return $result;
  }
}
